const Config = {
    env: 'local',
    local: {
        url: 'http://localhost:3000',
        RESTURL: 'http://localhost:8080'
    },
}

export default Config;
