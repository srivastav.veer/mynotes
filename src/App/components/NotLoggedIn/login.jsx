import React, { useState, useEffect, Fragment } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import '../../../css/login.css';
import Config from '../../../config';
import UserService from '../../service/userService';
import axios from 'axios';

export default function Login(props) {
    
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const [errorMsg, setErrMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const [btnDisabled, setBtnDisable] = useState(false);
    
    if (UserService.getUser()) {
        window.location = '/dashboard';
    }

    function login() {
        setBtnDisable(true);
        axios.post(Config[Config.env].RESTURL+'/user/login', {
            email: email,
            password: password
        },{
            timeout: 10000
        }).then(function(response) {
            setSuccessMsg('Login Successful');
            UserService.setLoggedInUser(response.data.data.data);
            window.location = '/dashboard';
        }).catch(e => {
            console.log(e.response);
            setErrMsg(e.response.data.message);
            setBtnDisable(false);
        });
    }
    return ( 
        <Fragment>
                <Modal show={true} id="loginModal" centered={true} className='outer-modal' onHide={() => {return false}}>
                <ModalHeader closeButton={true}>
                    <h4 className="montserrat-light">Login</h4>
                </ModalHeader>
                <Modal.Body className="padding-top-0">
                    <Form name="loginModalForm" id="loginModalForm">
                        <Form.Group controlId="loginModalForm__email">
                            <Form.Label className="padding-left-5">Email Id</Form.Label>
                            <Form.Control type="text" placeholder="Email Id" name="email_id" onChange={(e) => setEmail(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="loginModalForm__password">
                            <Form.Label className="padding-left-5">Your Password</Form.Label>
                            <Form.Control type="password" placeholder="Your Password" name="password" onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>
                        <div className="text-success font-weight-800 margin-bottom-10" id="success-message"></div>
                        {successMsg && 
                            <div className="text-success font-weight-800 margin-bottom-10" id="success-message">{successMsg}</div>
                        }
                        {errorMsg &&
                            <div className="text-x-love font-weight-800 margin-bottom-10" id="error-message">{errorMsg}</div>
                        }
                        <a className='' href='/signup'>Not a user? Register.</a>
                        <Form.Group controlId="loginModalForm__submitBtn">
                            <Button disabled={btnDisabled} variant="x-dark-default" className="right margin-left-5" id="loginModalForm__btnAddBrand" onClick={() => login()}>
                                Login
                            </Button>
                        </Form.Group>
                    </Form>
                </Modal.Body>
            </Modal>
        </Fragment>
    );
};