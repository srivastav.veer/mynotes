import React, { useState, Fragment } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import '../../../css/login.css';
import Config from '../../../config';
import axios from 'axios';

export default function SignUp(props) {
    
    const [firstName, setFistName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);
    const [cpassword, setCPassword] = useState(null);

    const [errorMsg, setErrMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const [btnDisabled, setBtnDisable] = useState(false);

    function signup() {
        setBtnDisable(true);

        if (password !== cpassword) {
            setErrMsg('Password Mismatch');
            setBtnDisable(false);
            return false;
        }

        axios.post(Config[Config.env].RESTURL+'/user', {
            first_name: firstName,
            last_name: lastName,
            email: email,
            password: password,
            cpassword: cpassword
        },{
            timeout: 10000
        }).then(function(response) {
            console.log(response);
            setSuccessMsg(response.data.message);
            setTimeout(function () {
                window.location = '/';
            }, 1500);
        }).catch(e => {
            console.log(e.response);
            setErrMsg(e.response.data.message);
            setBtnDisable(false);
        });
    }

    return ( 
        <Fragment>
                <Modal show={true} id="loginModal" centered={true} className='outer-modal' onHide={() => {return false}}>
                <ModalHeader closeButton={true}>
                    <h4 className="montserrat-light">Sign Up</h4>
                </ModalHeader>
                <Modal.Body className="padding-top-0">
                    <Form name="loginModalForm" id="loginModalForm">
                        <Form.Group controlId="loginModalForm__firstName">
                            <Form.Label className="padding-left-5">First Name</Form.Label>
                            <Form.Control type="text" placeholder="First Name" name="firstName" onChange={(e) => setFistName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="loginModalForm__lastName">
                            <Form.Label className="padding-left-5">Last Name</Form.Label>
                            <Form.Control type="text" placeholder="Last Name" name="lastName" onChange={(e) => setLastName(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="loginModalForm__email">
                            <Form.Label className="padding-left-5">Email Id</Form.Label>
                            <Form.Control type="email" placeholder="Email Id" name="email_id" onChange={(e) => setEmail(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="loginModalForm__password">
                            <Form.Label className="padding-left-5">Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" name="password" onChange={(e) => setPassword(e.target.value)} />
                        </Form.Group>
                        <Form.Group controlId="loginModalForm__cpassword">
                            <Form.Label className="padding-left-5">Retype Password</Form.Label>
                            <Form.Control type="password" placeholder="Confirm Password" name="cpassword" onChange={(e) => setCPassword(e.target.value)} />
                        </Form.Group>
                        {successMsg && 
                            <div className="text-success font-weight-800 margin-bottom-10" id="success-message">{successMsg}</div>
                        }
                        {errorMsg &&
                            <div className="text-x-love font-weight-800 margin-bottom-10" id="error-message">{errorMsg}</div>
                        }
                        <a className='' href='/'>Already have an acccount? Login.</a>
                        <Form.Group controlId="loginModalForm__submitBtn">
                            <Button disabled={btnDisabled} variant="x-dark-default" className="right margin-left-5" id="loginModalForm__btnAddBrand" onClick={() => signup()}>
                                Signup
                            </Button>
                        </Form.Group>
                    </Form>
                </Modal.Body>
            </Modal>
        </Fragment>
    );
};