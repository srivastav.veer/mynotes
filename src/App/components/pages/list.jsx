import React, { useState, Fragment } from 'react';

export default function Lists(props) {
    return ( 
        <Fragment>
            <div className="col col-4">
                <h3>{props.title}</h3>
                {props.children}
            </div>
        </Fragment>
    );
};