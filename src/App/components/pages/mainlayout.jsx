import React, { useState, Fragment } from 'react';
import Config from '../../../config';
import UserService from '../../service/userService';
import axios from 'axios';

import NavMenu from './nav/index';

export default function MainLayout(props) {
    if (!UserService.getUser()) {
        window.location = '/dashboard';
    }

    return ( 
        <Fragment>
            <NavMenu />
            <div className='container'>
                <div className='row'>
                    <div className='col col-12 content-holder'>
                        <h3 className='montserrat-light title-holder margin-bottom-20'>{props.title}</h3>
                        {props.children}
                    </div>
                </div>
            </div>
        </Fragment>
    );
};