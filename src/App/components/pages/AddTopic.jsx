import React, { useState, Fragment } from 'react';
import Button from 'react-bootstrap/Button';

export default function AddTopic(props) {
    return ( 
        <Fragment>
            <Button variant='primary' onClick={() => props.onClick()}>
                <i className="fas fa-plus-square"></i> Add Topic
            </Button>
        </Fragment>
    );
};