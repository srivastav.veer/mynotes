import React, { Fragment, Component } from 'react';
import Config from '../../../config';
import UserService from '../../service/userService';
import axios from 'axios';
import List from './list';
import AddTopic from './AddTopic';
import TopicModal from './topicModal';
import ListRender from './ListRender';
import DragDrop from './DragDrop';

import MainLayout from './mainlayout';
import NoteView from './notes/viewnote';

export default class Dashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            todoList: [],
            doingList: [],
            doneList: []
        }
    }

    componentDidMount() {
        if (!UserService.getUser()) {
            window.location = '/dashboard';
        }
    }
    
    onTopicCreate(t) {
        let list;
        switch (t.mode) {
            case 'todo':
                list = this.state.todoList.concat([
                    {
                        title: t.title,
                        details: t.details,
                        list: t.list
                    }
                ]);
                this.setState({todoList: list});
                break;
            case 'doing':
                list = this.state.doingList.concat([
                    {
                        title: t.title,
                        details: t.details,
                        list: t.list
                    }
                ]);
                this.setState({doingList: list});
                break;
            case 'done':
                list = this.state.doneList.concat([
                    {
                        title: t.title,
                        details: t.details,
                        list: t.list
                    }
                ]);
                this.setState({doneList: list});
                break;
        }
    }

    onChangeList(listName, indexList, toList) {
        // let souceList = getListFromString(listName)[0];
        // let destList = getListFromString(toList)[0];
        // let sourceListAction = getListFromString(listName)[1];
        // let destListAction = getListFromString(toList)[1];
        
        let souceList = this.state[listName+'List'];
        let destList = this.state[toList+'List'];
        let itemToMove = souceList.splice(indexList, 1);
        destList.push(itemToMove[0]);

        let stateUpdate = {};
        stateUpdate[`${listName}List`] = souceList;
        stateUpdate[`${toList}List`] = destList;

        this.setState(stateUpdate)
    }

    render() {
        return ( 
            <Fragment>
                <MainLayout title='My Task List'>
                    <TopicModal ref={(ref) => {this.addTopicModal=ref}}  onTopicCreate={(t) => this.onTopicCreate(t)} />
                    <div className="row">
                        <List title='to do'>
                            <AddTopic onClick={() => this.addTopicModal.toggleModal('todo')} />
                            <DragDrop as='todo' onChangeList={(ln, il, tl) => this.onChangeList(ln, il, tl)} />
                            <ListRender as='todo' topics={this.state.todoList} />
                        </List>
                        <List title='doing'>
                            <AddTopic onClick={() => this.addTopicModal.toggleModal('doing')} />
                            <DragDrop as='doing' onChangeList={(ln, il, tl) => this.onChangeList(ln, il, tl)} />
                            <ListRender as='doing' topics={this.state.doingList} />
                        </List>
                        <List title='done'>
                            <AddTopic onClick={() => this.addTopicModal.toggleModal('done')} />
                            <DragDrop  as='done' onChangeList={(ln, il, tl) => this.onChangeList(ln, il, tl)} />
                            <ListRender as='done' topics={this.state.doneList} />
                        </List>
                    </div>
                </MainLayout>
            </Fragment>
        );
    }
};