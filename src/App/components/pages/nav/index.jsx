import React, {Fragment} from 'react';
import UserService from '../../../service/userService';
import {Nav, Button, Dropdown} from 'react-bootstrap';

export default function MainNavBar (props) {
    function logout() {
        UserService.logout();
        window.location = "/";
    }
    
    return (
        <Fragment>
            <div className="nav-holder">
                <div className="container">
                    <div className="row">
                        <div className="col col-4 nav-logo">
                            <a className="logo" href="/"></a>
                        </div>
                        <div className="col col-8">
                            <button className='right btn btn-primary margin-left-20' onClick={() => logout()}>Logout</button>
                            <Dropdown>
                                <Dropdown.Toggle variant="primary" id="dropdown-basic" className='right'>
                                    <i className="fas fa-plus margin-right-10"></i> Notes
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <Dropdown.Item href="/notes/create/text">Text Notes </Dropdown.Item>
                                    <Dropdown.Item href="/notes/create/list">Lists</Dropdown.Item>
                                    {/* <Dropdown.Item href="/notes/create/map">Map</Dropdown.Item> */}
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}