import React, { useState, useEffect, Fragment } from 'react';
import Form from 'react-bootstrap/Form';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from "draft-js";
import {Button} from 'react-bootstrap';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import Config from '../../../../config';
import UserService from '../../../service/userService';
import axios from 'axios';

import MainLayout from '../mainlayout';

export default function CreateList(props) {
    var autoSaveTimeout;
    var itemRef = React.createRef();
    const [id, setId] = useState(props.id);
    const [title, setTitle] = useState(null);
    const [list, setList] = useState([]);
    const [btnDisabled, setBtnDisable] = useState(false);

    useEffect(() => {
        autoSaveTimeout = setTimeout(async function () {
            try {
                await autoSave();
            } catch (e) {
                console.log(e);
            }
        }, 1000);
        return () => clearTimeout(autoSaveTimeout);
    });

    function addItem() {
        let newL = [itemRef.current.value];
        setList(list.concat(newL));
        itemRef.current.value = '';
    }

    function autoSave() {
        let htmlContent = JSON.stringify(list);
        var data = {
            topicName: title,
            details: htmlContent,
            topicType: props.type
        }
        return new Promise (function (resolve, reject) {
            if (!id) {
                if (!title) {
                    return reject();
                }

                createNewTopic(data).then(() => {
                    return resolve();
                }).catch(e => {
                    return reject();
                })
            } else {
                data.id = id;
                console.log('Save with Id', data);
                updateTopic(data).then(() => {
                    return resolve();
                }).catch(e => {
                    return reject();
                })
            }
        });
    }

    function done() {
        autoSave().then(() => {
            window.location = "/dashboard";
        }).catch(e => {
            alert('Some Error');
        })
    }

    function updateTopic(data) {
        setBtnDisable(false);
        console.log(data);
        return new Promise (function (resolve, reject) {
            axios.put(Config[Config.env].RESTURL+'/topic/'+id, data ,{
                headers: {
                    'x-auth': UserService.getUserToken()
                },
                timeout: 10000
            }).then(function(response) {
                setBtnDisable(false);
                resolve();
            }).catch(e => {
                console.log(e.response);
                setBtnDisable(false);
                reject();
            });
        });
    }

    function createNewTopic(data) {
        setBtnDisable(true);
        return new Promise (function (resolve, reject) {
            axios.post(Config[Config.env].RESTURL+'/topic', data ,{
                headers: {
                    'x-auth': UserService.getUserToken()
                },
                timeout: 10000
            }).then(function(response) {
                setId(response.data.id);
                setBtnDisable(false);
                resolve();
            }).catch(e => {
                console.log(e.response);
                setBtnDisable(false);
                reject();
            });
        });
        
    }

    return ( 
        <Fragment>
            <MainLayout title={props.title}>
                <Form name="createTopicForm" id="createTopicForm">
                    <Form.Group controlId="createTopicForm__title">
                        <Form.Label className="padding-left-5">Notes Title</Form.Label>
                        <Form.Control type="text" placeholder="Title" name="title" onChange={(e) => setTitle(e.target.value)} />
                    </Form.Group>
                    <Form.Group controlId="createTopicForm__Notes">
                        <div className='row'>
                            <div className='col col-7'>
                                <Form.Control type="text" placeholder="Item To Add" name="items" ref={itemRef} />
                            </div>
                            <div className='col col-5'>
                                <Button onClick={(e) => addItem(e.target.value)}>+ Add</Button>
                            </div>
                        </div>
                    </Form.Group>
                    <ul>
                        {list.map((listItem, key) => {
                            return (<li key={key}>{listItem}</li>)
                        })}
                    </ul>
                    <Form.Group controlId="loginModalForm__submitBtn">
                        <Button disabled={btnDisabled} variant="x-dark-default" className="right margin-left-5" id="loginModalForm__btnAddBrand" onClick={() => done()}>
                            Done
                        </Button>
                    </Form.Group>
                </Form>
            </MainLayout>
        </Fragment>
    );
};