import React, { useState, Fragment } from 'react';

export default function NoteView(props) {

    function renderList(details) {
        details = JSON.parse(details);
        return (
            <Fragment>
                <ul>
                    {details.map((detail, key) => {
                        return (<li key={key}>{detail}</li>);
                    })}
                </ul>
            </Fragment>
        );
    }

    function openNote() {

    }
    console.log(props);
    return ( 
        <Fragment>
            <div className='notes' onClick={() => openNote()}>
                <h5>{props.topic.topic_name}</h5>
                {props.topic.topic_type === 'text' &&
                    <div dangerouslySetInnerHTML={{__html: props.topic.details}} />
                }
                {props.topic.topic_type === 'list' &&
                    renderList(props.topic.details)
                }
            </div>
        </Fragment>
    );
};