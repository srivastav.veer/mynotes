import React, {Fragment, Component} from 'react';
import {Card} from 'react-bootstrap';

export default class DragDrop extends Component {
    
    constructor(props) {
        super(props);
    }

    onDragOver(e) {
        //let event = e as Event;
        e.stopPropagation();
        e.preventDefault();
    }

    onDrop(e) {
        let listName =  e.dataTransfer.getData('list');
        let listIndex = e.dataTransfer.getData('itemIndex');
        // console.log('onDrop', e.dataTransfer.getData('itemIndex'), this.props.as);
        console.log(this.props);
        this.props.onChangeList(listName, listIndex, this.props.as);
    }

    // onDragEnter(e) {
    //     console.log(e.dataTransfer);
    //     console.log('drag enter ', e.dataTransfer.getData('itemIndex'));
    // }

    render() {
        return ( 
            <Fragment>
                <Card className='margin-top-10'
                    // onDragEnter={(e) => this.onDragEnter(e)}
                    onDragOver={(e) => this.onDragOver(e)}
                    onDrop={(e) => this.onDrop(e)}>
                    <Card.Body>
                        <h5>Drag Here</h5>
                    </Card.Body>
                </Card>
            </Fragment>
        );
    }
};