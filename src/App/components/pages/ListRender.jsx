import React, {Fragment, Component} from 'react';
import {Card} from 'react-bootstrap';

export default class ListRender extends Component {
    
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            list: []
        }
    }

    componentDidMount() {
        console.log(this.props);
        this.updateList(this.props);
    }

    updateList(data) {
        this.setState({list: data.topics});
    }

    componentWillReceiveProps(nextProps) {
        console.log('nextProps', nextProps);
        this.updateList(nextProps);
    }

    onDragStart(e, t) {
        //console.log('dragStart ', e , t , this.props);
        e.dataTransfer.setData('itemIndex', t);
        e.dataTransfer.setData('list', this.props.as);
        // console.log(e.dataTransfer);
        // console.log(e.dataTransfer.getData('itemIndex'));
    }

    render() {
        return ( 
            <Fragment>
                {this.state.list.map((l,i) => {
                    return (
                        <Card className='margin-top-10' key={i} 
                            draggable={true} 
                            onDragStart={(e) => this.onDragStart(e, i)}
                            >
                            <Card.Body>
                                <h4>{l.title}</h4>
                                <p>{l.details}</p>
                            </Card.Body>
                        </Card>
                    );
                })}
            </Fragment>
        );
    }
};