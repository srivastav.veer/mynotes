import React, { Component, Fragment } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import {Form, Button} from 'react-bootstrap';

export default class TopicModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            details: '',
            modalShow: false,
            mode: 'todo'
        }
    }

    add() {
        this.props.onTopicCreate({title: this.state.title, details: this.state.details, mode: this.state.mode });
        this.setState({modalShow: !this.state.modalShow});
    }

    toggleModal(mode) {
        mode = mode || null;
        this.setState({modalShow: !this.state.modalShow, mode: mode});
    }

    render() {
        return ( 
            <Fragment>
                    <Modal show={this.state.modalShow} id="createTopicModal" centered={true} className='outer-modal' onHide={() => {return false}}>
                    <ModalHeader closeButton={true}>
                        <h4 className="montserrat-light">Add Topic</h4>
                    </ModalHeader>
                    <Modal.Body className="padding-top-0">
                        <Form name="createTopicModal" id="createTopicModal">
                            <Form.Group controlId="createTopicModal__title">
                                <Form.Label className="padding-left-5">Title</Form.Label>
                                <Form.Control type="text" placeholder="Title" name="title" onChange={(e) => this.setState({title: e.target.value})} />
                            </Form.Group>
                            <Form.Group controlId="createTopicModal__details">
                                <Form.Label className="padding-left-5">Details</Form.Label>
                                <Form.Control as="textarea" placeholder="Details" name="details" onChange={(e) => this.setState({details: e.target.value})} />
                            </Form.Group>
                            <Form.Group controlId="createTopicModal__submitBtn">
                                <Button variant="x-dark-default" className="right margin-left-5" id="createTopicModal__submitBtn" 
                                    onClick={() => this.add()}>
                                    Add Topic
                                </Button>
                                <Button variant="danger" className="right margin-left-5" id="createTopicModal__cancelBtn" 
                                    onClick={() => this.toggleModal()}>
                                    Cancel
                                </Button>
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                </Modal>
            </Fragment>
        );
    }
};