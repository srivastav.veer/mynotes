import React, { useState, Fragment } from 'react';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";

import Config from '../../../config';
import UserService from '../../service/userService';
import axios from 'axios';

import NavMenu from './nav/index';

export default function MainPage(props) {
    if (!UserService.getUser()) {
        window.location = '/';
    }

    return ( 
        <Fragment>
            <NavMenu />
            <div className='container'>
                <div className='row'>
                    <div className='col col-12 content-holder'>
                        <h3 className='montserrat-light title-holder margin-bottom-20'>{props.title}</h3>
                        {props.children}
                    </div>
                </div>
            </div>
        </Fragment>
    );
};