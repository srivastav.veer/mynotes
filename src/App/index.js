import React, {Fragment} from 'react';
import { BrowserRouter, Route, Link, Switch } from "react-router-dom";
import '../css/app.css';

import Login from './components/NotLoggedIn/login';
import SignUp from './components/NotLoggedIn/signup';
import Dashboard from './components/pages/dashboard';
import ErrorPage from './components/errorPage';
import CreateNotes from './components/pages/notes/create_notes';
import CreateList from './components/pages/notes/create_list';

export default function Index(props) {
    return (
        <Fragment>
            <BrowserRouter>
                <Switch>
					{/* Login Screen */}
                    <Route exact path="/" component={Login} /> 
					{/* Sign Up */}
                    <Route exact path="/signup" component={SignUp} /> 
					{/* Dashboard */}
					<Route exact path="/dashboard" component={Dashboard} /> 
                    <Route exact path="/notes/create/text" component={() => <CreateNotes title='Make a Text Note' type='text' />} /> 
                    <Route exact path="/notes/create/list" component={() => <CreateList title='Make a List' type='list' />}/> 
                    {/* <Route exact path="/notes/create/map" component={() => <CreateNotes title='Save a Map Location' type='map' />} />  */}
                    <Route component={ErrorPage} />
                </Switch>
            </BrowserRouter>
        </Fragment>
    );
}