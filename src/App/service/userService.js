import axios from 'axios';
import Config from '../../config';

var user = null;

const UserService = (function () {

    var setLoggedInUser = function(_user) {
        user = _user;
        user.token = _user.login_auth;
        user.topics = _user.UserTopics;

        delete user.login_auth;

        localStorage.setItem('userId', user.id);
        localStorage.setItem('token', user.token);
        localStorage.setItem('user', JSON.stringify(user));
    };

    var getUserToken = function() {
        if (!user) {
            return null;
        }
        return user.token;
    };

    var getUser = function() {
        if (user == null) {
            user = JSON.parse(localStorage.getItem('user'));
        }
        return user;
    };

    var logout = function() {
        localStorage.clear();
    };

    getUser();

    if (user) {
        axios.post(Config[Config.env].RESTURL+'/user/verifyAuth', {
            userId: user.id,
            token: user.token
        },{
            timeout: 10000
        }).then(function(response) {
            setLoggedInUser(response.data.data.data);
        }).catch(e => {
            console.log(e.response);
            window.location = "/";
            user = null;
            localStorage.clear();
        });
    }

    return {
        getUserToken: getUserToken, //Get User Auth Token
        getUser: getUser, //Get the user details
        setLoggedInUser: setLoggedInUser,
        logout: logout
    }
})();

export default UserService;